#!/usr/bin/env python3
import pika
import sys
import random
import time

credentials = pika.PlainCredentials('root', 'dsv')
parameters = pika.ConnectionParameters('127.0.0.1', 5672, '/', credentials)
connection = pika.BlockingConnection(parameters)

channel = connection.channel()
# durable = exists until broker restarts
channel.queue_declare(queue='numbers', durable=True)

while True:
    message = str(random.randint(0, 9))
    channel.basic_publish(
        exchange='',
        routing_key='numbers',
        body=message,
        properties=pika.BasicProperties(
            delivery_mode=2,
        ))
    print(f" [x] Sent {int(message)}")
    time.sleep(1)

connection.close()
#!/usr/bin/env python3
import pika
import time
import random

credentials = pika.PlainCredentials('root', 'dsv')
parameters = pika.ConnectionParameters('127.0.0.1', 5672, '/', credentials)
connection = pika.BlockingConnection(parameters)

channel = connection.channel()
channel.queue_declare(queue='numbers', durable=True)

channel_prime = connection.channel()
channel_prime.queue_declare(queue='primes', durable=True)

print(' [*] Waiting for messages. To exit press CTRL+C')

def is_prime(n: int):
    if n > 1:
        for i in range(2, int(n/2) + 1):
            if (n % i) == 0:
                return False
        return True
    else:
        return False


def send_number(n: int):
    message = str(n)
    channel_prime.basic_publish(
        exchange='',
        routing_key='primes',
        body=message,
        properties=pika.BasicProperties(
            delivery_mode=2,
        ))

def callback(ch, method, properties, body):
    print(f" [x] Received {int(body)}")

    if is_prime(int(body)):
        send_number(int(body))

    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='numbers', on_message_callback=callback)
channel.start_consuming()
